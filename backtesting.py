# LIBRARIES
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# IMPORT DATA
data = pd.read_csv('data_eth.csv')
tick_interval = '15m'

# VARIABLES
moving_average = 100
stoploss = 0.03
slope_buy_signal = 0

# TECHNICAL INDICATORS BUY AND SELL SIGNALS
# Moving Average
data['ma'] = data['close'].rolling(window=moving_average).mean()

# Slope
data['slope'] = np.log(data['ma'] / data['ma'].shift())

# Buy signal = 1
data['buy_signal'] = np.where((data['ma'].shift(1) < data['ma']) &
                              (data['ma'].shift(2) > data['ma'].shift(1)) &
                              (data['ma'] < data['close']) &
                              (data['slope'] > slope_buy_signal), 1, 0)

# Sell signal = 2
data['sell_signal'] = np.where((data['ma'].shift(1) > data['ma']) &
                               (data['ma'].shift(2) < data['ma'].shift(1)) &
                               (data['ma'] > data['close']) &
                               (data['slope'] < 0), 2, 0)

# SIGNALS
data['signals'] = data['buy_signal'] + data['sell_signal']

# STRATEGY
df = data.copy()
df = df.fillna(0)

# Variables
capital = 10000
commission = 0.003
quantity = 0
cumulative_profit = 0
buy_price = 0
sell_price = 0
in_position = False

# Columns
df['capital'] = 0
df['sell'] = 0
df['profit'] = 0
df['cumulative_profit'] = 0
df['actions'] = 0
df['money_strategy'] = capital

# Backtesting
for index, row in df.iterrows():
    print(index)
    action = row['signals']
    price = row['close']
    low = row['low']

    # Buy
    if (action == 1) & (not in_position):
        # Remove commissions to make sure Wwe can pay them
        quantity = (capital - (capital * commission)) / price
        print(quantity)
        # Check if we can buy
        if (quantity == 0) | (capital <= capital * commission + price):
            print(f"Insufficient funds. Balance: {capital}. Price: {price}")
            continue
        capital -= quantity * price + (capital * commission)
        buy_price = price
        in_position = True
        df.loc[index, 'buy_price'] = buy_price
        stoploss_price = buy_price - (buy_price * stoploss)
        print('Buy!')

    # Sell
    if in_position:
        if action == 2:
            capital += quantity * price - (capital * commission)
            profit = (price - buy_price) * quantity
            cumulative_profit += profit - (capital * commission)
            df.loc[index, 'profit'] = round(profit, 2)
            sell_price = price
            df.loc[index, 'sell_price'] = sell_price
            in_position = False
            print('Sell!')

        elif low < stoploss_price:
            capital += quantity * stoploss_price - (capital * commission)
            profit = (stoploss_price - buy_price) * quantity
            cumulative_profit += profit - (capital * commission)
            df.loc[index, 'profit'] = round(profit, 2)
            sell_price = stoploss_price
            df.loc[index, 'sell_price'] = sell_price
            in_position = False
            print('Stoploss filled')

    df.loc[index, 'actions'] = quantity
    df.loc[index, 'capital'] = round(capital, 2)
    df.loc[index, 'cumulative_profit'] = round(cumulative_profit, 2)

df['money_strategy'] = df['money_strategy'] + df['cumulative_profit']
money_strategy = df['money_strategy']


# Equity curve
def eq_curve(equity_curve=money_strategy):
    plt.title('Equity Curve Strategy')
    plt.xlabel('tick_interval: 15m')
    plt.ylabel('money')
    equity_curve.plot(color='magenta', alpha=0.5)


eq_curve()

# Positive and negative trades
pos_trades = np.where(df['profit'] > 0, 1, 0).sum()
neg_trades = np.where(df['profit'] < 0, 1, 0).sum()

list_trades = pos_trades, neg_trades
labels = ['positive trades', 'negative trades']


def plot_bars(data, y_label, x_label, labels):
    frequencies = pd.DataFrame(data)
    ax = frequencies.plot(kind='bar', color='cornflowerblue')
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_xticklabels(labels, rotation='horizontal')
    ax.legend().remove()
    rects = ax.patches

    for rect, benef in zip(rects, data):
        height = rect.get_height()
        if height > 0:
            ax.text(rect.get_x() + rect.get_width() / 2, height + (height * 0.02), "%.0f" % benef,
                    ha='center', va='bottom', fontsize=12)
        else:
            ax.text(rect.get_x() + rect.get_width() / 2, height - 8, "%.0f" % benef,
                    ha='center', va='bottom', fontsize=12)
    if (min(data) <= 0) & (max(data) >= 0):
        plt.ylim([min(data) + (min(data) * 0.5), max(data) + (max(data) * 0.4)])
    elif (max(data) == 0):
        plt.ylim([min(data) + (min(data) * 0.2), -(min(data) * 0.2)])
        ax.legend().remove()
    else:
        plt.ylim([0, max(data) + (max(data) * 0.4)])

plot_bars(data=list_trades, y_label= 'trades', x_label='', labels=labels)



def buy_sell_graph(tick_interval='15m'):
    df_graph = df.copy()
    df_graph['buy_price'] = df_graph['buy_price'].replace(0, np.nan)
    df_graph['sell_price'] = df_graph['sell_price'].replace(0, np.nan)
    plt.figure(figsize=(15, 7))
    plt.title('Buy and sell signals ETHUSDT')
    plt.xlabel(f'tick_interval: {tick_interval}')
    plt.ylabel('close')
    plt.scatter(df_graph.index, df_graph['buy_price'], color='green', label='buy', marker='^', alpha=1)
    plt.scatter(df_graph.index, df_graph['sell_price'], color='red', label='sell', marker='v', alpha=1)
    close = df_graph['close']
    ma = df_graph['ma']
    close.plot(alpha=0.5)
    ma.plot(alpha=0.5)
    plt.legend()


buy_sell_graph()
