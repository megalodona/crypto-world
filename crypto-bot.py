# LIBRARIES
import websocket, json
import numpy as np
import pandas as pd
from binance.enums import *
import requests
from datetime import datetime
import config
from binance.client import Client
import time


# What do yo want to trade?
symbol = 'ethusdt'
tick_interval = '1m'

# CLIENT
SOCKET = 'wss://stream.binance.com:9443/ws/' + symbol + '@kline_' + tick_interval
client = Client(config.API_KEY, config.API_SECRET, tld='com')


# VARIABLES
closes = []
buy_prices = []
buy_quantity = []
in_position = False


# TELEGRAM MESSAGES
def telegram_bot_sendtext(bot_message):
    bot_token = config.BOT_TOKEN_TELEGRAM
    bot_chatID = config.CHATID_TELEGRAM
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + \
                '&parse_mode=Markdown&text=' + bot_message
    response = requests.get(send_text)
    return response.json()


# BALANCE ACCOUNT
def get_balance(coin='USDT'):
    balances = client.get_account()['balances']
    for b in balances:
        if (float(b["free"]) > 0) & (b['asset']==coin):
            balance = float(b["free"])
    return balance


# INFORMATION TRADES
def print_trades(symbol='ETHUSDT'):
    trades = client.get_my_trades(symbol=symbol)
    balance_account = get_balance()
    for trade in trades:
        price = float(trade['price'])
        quantity = float(trade['qty'])
        buy = trade['isBuyer']
        commission = float(trade['commission'])
        symbol_t = trade['symbol']
        time = str(datetime.fromtimestamp(trade['time'] / 1000))
        total = price*quantity - commission
        if buy:
            info_ = f"[{time}]// Buy {quantity} {symbol_t} // " \
                    f"Price: {price} // " \
                    f"Total amount: {round(total, 2)} // " \
                    f"Balance account: {round(balance_account, 2)}USDT"
        else:
            info_ = f"[{time}]// Sell {quantity} {symbol_t} // " \
                    f"Price: {price} // " \
                    f"Total amount: {round(total, 2)} // " \
                    f"Balance account: {round(balance_account, 2)}USDT"
    return info_


# SEND STOPLOSS ORDER
def send_stoploss_order(symbol='ETHUSDT', stopprice=1701, price=1701, quantity=0.01):
    client.create_order(symbol=symbol, side=SIDE_SELL, type=ORDER_TYPE_STOP_LOSS_LIMIT, quantity=quantity,
                        timeInForce='GTC', stopPrice=stopprice, price=price)

# CANCEL ORDER
def cancel_order (symbol='ETHUSDT'):
    open_orders = client.get_open_orders()
    for order in open_orders:
        id_ = order['orderId']
    client.cancel_order(symbol=symbol, orderId=id_)

# OPEN AND CLOSE CONNECTION
def on_open(ws):
    print('opened connection')

def on_close(ws):
    print('closed connection')


# STRATEGY
def on_message(ws, message, mov_avg_short=70, money=50, trade_symbol='ETHUSDT', stoploss=0.02, slope_buy_signal=0.05):

    global in_position, buy_prices, buy_quantity, closes
    json_message = json.loads(message)
    candle = json_message['k']
    is_candle_closed = candle['x']
    close = candle['c']

    if is_candle_closed:
        data = pd.DataFrame(columns=['close', 'ma', 'slope', 'buy_signal'])
        closes.append(float(close))
        data['close'] = closes
        data['ma'] = data['close'].rolling(window=mov_avg_short).mean()
        data['slope'] = (np.log(data['ma'] / data['ma'].shift())) * 1000
        data['buy_signal'] = np.where((data['ma'].shift(1) < data['ma']) &
                                      (data['ma'].shift(2) > data['ma'].shift(1)) &
                                      (data['ma'] < data['close']) &
                                      (data['slope'] > slope_buy_signal), 1, 0)
        print(data)

        # check if stoploss is filled
        open_order = client.get_open_orders()
        if (not open_order) and in_position:
            in_position = False
            print('Stoploss filled')
            # send message (stoploss)
            trades = print_trades()
            telegram_bot_sendtext(trades)
            print(trades)

        close = data['close'].iloc[[-1]]
        close = close.tolist()[0]
        action = data['buy_signal'].iloc[[-1]]
        action = action.tolist()[0]
        slope = data['slope'].iloc[[-1]]
        slope = slope.tolist()[0]

        # BUY
        if (action == 1) & (not in_position):
            print("Buy!")
            buy_prices.append(close)
            # quantity to buy
            trade_quant = (money / close)
            trade_quant = round(trade_quant, 4)
            buy_quantity.append(trade_quant)
            # send buy order
            client.order_market_buy(quantity=trade_quant, symbol=trade_symbol)
            in_position = True
            time.sleep(3)
            # set stoploss
            stopprice = close*(1-stoploss)
            stopprice = (round(stopprice, 2))
            # send stoploss
            send_stoploss_order(stopprice=stopprice, price=stopprice, quantity=trade_quant)
            print("Stoploss order sent")
            # send message (buy)
            trades = print_trades()
            telegram_bot_sendtext(trades)
            print(trades)


        # SELL
        if in_position & (slope < 0):
            print('Sell!')
            sell_quantity = buy_quantity[-1]
            # send sell order
            client.order_market_sell(quantity=sell_quantity, symbol=trade_symbol)
            in_position = False
            time.sleep(3)
            # send message (sell)
            trades = print_trades()
            telegram_bot_sendtext(trades)
            # cancel stoploss order
            cancel_order()
            print('Stoploss order canceled')
            print(trades)


ws = websocket.WebSocketApp(SOCKET, on_open=on_open, on_message=on_message,  on_close=on_close)
ws.run_forever()
