# Automated cryptocurrency trading bot 

This code is an investment strategy that serves as the basis for the development of more complex strategies that could take additional personalized factors into account. I used Python, WebSocket’s and the Binance API to retrieve real-time price data and apply technical indicators to the same data in order to execute trades automatically. I also created a Telegram based chatbot that keeps me updated on the actions taken by the trading bot.

Binance API: allows us to connect to the Binance servers via Python or several other programming languages. Once we install the library (binance-python) and obtain the API keys we can start up a client, send orders, check out our account balance, etc.

Binance WebSocket: requires a command to be sent once to open up a stream so that the data will automatically stream over as prices are updated.
Telegram Bot: You can integrate a bot with a Telegram bot that will notify you with the personalized information chosen for each trade like the price, quantity, balance account, profit, etc.  This is done by creating a bot using Telegram’s BotFather and inserting the Telegram Bot’s TOKEN and the corresponding CHAT_ID in the code. 

Create an account in Binance: https://www.binance.us/en/home

Create a bot in telegram with BotFather: https://core.telegram.org/bots


## Variables of the strategy
-	Money: capital on each trade
-	Moving average: 70 bars
-	Stop-Loss: 2%
-	Slope_buy_signal > 0
-   Symbol: ETHUSDT

## Strategy
The strategy consists of buying ETHUSDT (or any coin) when the slope of the moving average changes from negative to positive (slope_buy_signal > 0) forming “a valley”. The sell signal will be triggered when the moving average forms a “vertex”, meaning when the slope changes from positive to negative.  A static stop-loss sell signal is also put in place for capital protection.


<img src="images/buy_signal.png"  width="700" height="350">

# Backtesting Results
A python based backtesting file is applied to the data set using the trading strategy to ascertain the performance of the designed investment strategy over time.  The program provides buy-sell signals of the underlying asset in a graph, positive and negative trades and an equity curve.

## Data information
-	Historical data 
-	Crypto: ETHUSDT 
-	Tick interval: 15 minutes
-	Dates: from 2017-08-17 to 2021-03-15

## Buy-sell signals
Here is an example of buy and sell signals in a short period of time (2020-08-10 to 2020-09-10):
![](images/buy_sell_signals_example.png)

## Positive and negative trades
![](images/pos_neg_trades.png)

## Equity Curve
This is the Equity curve of the strategy considering initial investing 10.000$ and reinvesting profits in each trade.
![](images/eq_curve.png)

IMPORTANT: This code is just a simple example of a trading strategy, IT’S NOT PROFITABLE! 



